#include<stdio.h>
int input()
{
 float r;
 printf("enter the radius");
 scanf("%f",&r);
 return r;
}
float to_find_area(float r)
{
 float area;
 area=r*r*3.14;
 return area;
}
void output(float r,float area)
{
 printf("the area of a circle with radius %f is %f",r,area);
}
int main()
{
 float r;float area;
 r=input();
 area=to_find_area(r);
 output(r,area);
 return 0;
 }