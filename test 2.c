#include<stdio.h>
struct time
{
  int hours;
  int minutes;
};
typedef struct time Time;
Time input()
{
  Time t;
  printf("Enter time in hours:minutes format");
  scanf("%d:%d",&t.hours,&t.minutes);
  return t;
}
int convert_time(Time t)
{
 int r;
 r=(60*t.hours)+(t.minutes);
 return r;
}
void output(Time t,int r)
{
 printf("the answer in minutes is %d\n",r);
}
 
int main()
{
 Time t;int r;
 t=input();
 r=convert_time(t);
 output(t,r);
 return 0;
}