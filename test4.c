#include<stdio.h>
int input()
{
 int n;
 printf("enter the number");
 scanf("%d",&n);
 return n;
}
int to_find_sum_of_digits(int n)
{
 int i=n;
 int q,r;
 int sum=0;
 while(i!=0)
 {
  q=i/10;
  r=i%10;
  sum+=r;
  i=q;
  }
  return sum;
 }
void output(int n,int sum)
{
 printf("the sum of digits of number %d is %d\n",n,sum);
}
int main()
{
 int n;int sum;
 n=input();
 sum=to_find_sum_of_digits(n);
 output(n,sum);
 return 0;
}